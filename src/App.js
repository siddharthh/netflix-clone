// import logo from './logo.svg';
import "./App.css";
import Banner from "./components/Banner";
import Row from "./components/Row";
import requests from "./modules/requests.js";
import Nav from "./components/Nav";

function App() {
  return (
    <div className="app">
      <Nav />
      <Banner />
      <Row
        title={"NETFLIX ORIGINALS"}
        fetchUrl={requests.fetchNetflixOriginals}
        isLargeRow
      ></Row>
      <Row title={"Trending"} fetchUrl={requests.fetchTrending}></Row>
      <Row title={"Top Rated"} fetchUrl={requests.fetchTopRated}></Row>
      <Row title={"Action Movies"} fetchUrl={requests.fetchActionMovies}></Row>
      <Row title={"Comedy Movies"} fetchUrl={requests.fetchComedyMovies}></Row>
      <Row title={"Horror Movies"} fetchUrl={requests.fetchHorrorMovies}></Row>
      <Row
        title={"Romance Movies"}
        fetchUrl={requests.fetchRomanceMovies}
      ></Row>
      <Row title={"Documentaries"} fetchUrl={requests.fetchDocumentaries}></Row>
    </div>
  );
}

export default App;
